current=$(pwd);
cd "$1"
prusaresult=$(prusa-slicer --filament-cost 100/1 --filament-density 1.24 --support-material-auto --export-3mf --export-gcode --slice "$2.$3")
used=$(cat "$2.gcode" | grep "filament used \[cm3\]" | awk '{print $6}')
echo $used
cd $current
