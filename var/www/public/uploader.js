var ajax = new XMLHttpRequest;

ajax.upload.addEventListener("progress", upload_progress, false);
ajax.addEventListener('load', upload_complete, false);
ajax.onreadystatechange = (event) => {
    if(ajax.readyState == XMLHttpRequest.DONE) {
        postMessage({
            event: "response",
            payload: JSON.parse(ajax.responseText)
        });
    }
};

onmessage = (event) => {
    console.log("worker running now");
    ajax.open('POST', '/submitprint', true);
    ajax.send(event.data.formdata);
}

function upload_progress(event) {
    postMessage({
        event: "uploadProgress",
        payload: (event.loaded / event.total) * 100
    });
}

function upload_complete() {
    postMessage({
        event: "uploadComplete",
        payload: null
    });
}