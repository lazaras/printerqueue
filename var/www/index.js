/*
copyright (C) 2022 all rights reserved
the use, or distribution of this code by any entity other than Julian Eliot Lazaras
is strictly prohibited and will be seen as intellectual property theft
*/
const { v4 } = require('uuid');

const exec = require("child_process").exec;
const express = require("express");
const path = require("path");
const router = express();
const ini = require('ini');

const { Client } = require('pg');
const fs = require("fs");
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const passwd = fs.readFileSync("pgpasswd.txt").toString().replace(/(\r\n|\n|\r)/gm, "");
//const index_file = fs.readFileSync("public/index.html");

router.set("view engine", "ejs");
router.use(express.static("public/"));

const datadir = process.env.DATADIR;
if(datadir == null) {
    datadir = __dirname;
}

router.use(fileUpload({
    createParentPath: true
}));

//add other middleware
router.use(cors());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

const args = {
    user: "eplstore",
    host: "db.cecs.pdx.edu",
    database: "eplstore",
    password: process.env.DBPASSWD,
    port: 5432,
    ssl: {
        rejectUnauthorized: false,
        key: fs.readFileSync("./ssl-cert-snakeoil.key").toString(),
        cert: fs.readFileSync("./ssl-cert-snakeoil.pem").toString(),
    }
};

var example_configs = {
    listenport: 3456,
    uploaddir: __dirname + "/uploads/",
    postgres: {
        user: "",
        host: "",
        database: "",
        password: "",
        port: 5432,
    },

};

const client = new Client(args);

client.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

router.get("/", function (req, res) {
    res.render('index', null);
    //res.sendFile(path.join(__dirname, "public/index.html"));
});

router.get("/listcosts", (req, res) => {
    client.query("select * from material_costs", null, (err, results) => {
        if(err != null) {
            console.log(err);
        }else{
            res.send(JSON.stringify(results.rows));
        }
    });
});

router.post("/submitprint", async (req, res) => {
    console.log("post received");
    try {
        if(!req.files) {
            res.send("please make sure to upload a file");
            res.end();
        }else{
            //console.log(JSON.stringify(req));
            let print = req.files.print;
            let id = v4();
            console.log("print name: " + print.name);
            let upload_path = datadir + "/uploads/" + req.body.email + "/" + id + "/";
            print.mv(upload_path + print.name);
            let parts = print.name.split(".");
            let format = parts[parts.length - 1];
            parts.pop();
            let base = parts.join();
            console.log("file base: " + base + " extension: " + format);
            let cmd = "./prusa-price.sh " + upload_path + " " + base + " " + format;
            let file = upload_path + base;
            let input_file = file + "." + format;
            let sliced_file = file + ".3mf";
            console.log("command: " + cmd);

            let email = req.body.email;
            let userid = req.body.userid;
            let name = req.body.name;
            let material = req.body.material;
            if(email == null) {
                res.send({
                    status: "failure",
                    reason: "missing email"
                });
            }
            if(userid == null) {
                res.send({
                    status: "failure",
                    reason: "missing userid"
                });
            }

            if(name == null) {
                res.send({
                    status: "failure",
                    reason: "missing name"
                });
            }

            if(material == null) {
                res.send({
                    status: "failure",
                    reason: "missing material"
                });
            }

            exec(cmd, async (err, stdout, stderr) => {
                if(err != null && stderr != null) {
                    console.log("I'll deal with errors later");
                    res.end();
                }
                console.log("stdout: " + stdout);
                let price = Math.ceil((0.124 * stdout) * 100) / 100;
                
                await client.query("insert into queue (id, printname, material, userid, input_file, sliced_file, email, estimate) values ($1, $2, $3, $4, $5, $6, $7, $8)", [id, name, material, userid, input_file, sliced_file, email, price]);

                res.send(JSON.stringify({
                    status: "success",
                    id: id
                }));
            });

            /*command.stdout.on('data', async (stdout) => {
                console.log("stdout: " + stdout);
                let price = Math.ceil((0.124 * stdout) * 100) / 100;
                let id = v4();
                await client.query("insert into queue (id, userid, input_file, sliced_file, email, estimate) values ($1, $2, $3, $4, $5, $6)", [id, userid, input_file, sliced_file, email, price]);

                render_accept(id, res);

                console.log("estimated price: " + price);
            });
            command.stderr.on('data', (stderr) => {
                console.log("stderr: " + stderr);
            });

            command.on('exit', function(code){
                //console.log("stdout: " + JSON.stringify(command.stdout));
            });
            
            console.log("price: " + price);
            //client.query("insert into queue (id, ");
            res.send("successully uploaded");*/
        }
    }catch(err) {
        res.status(500).send(err);
    }
});

let render_accept = async (id, resp) => {
    // to_char is used to format the date in a more user friendly way then showing full timezone data
    client.query("select printname, to_char(submitted, 'mm/dd/yyyy') as submitted, materialname as material, estimate from queue, material_costs where material_costs.id = queue.material and queue.id = $1", [id], (err, res) => {
        
        if(err != null) {
            console.log(err);
            resp.end();
        }
        let result = res.rows;
        console.log("result length: " + result.length);
        if(result == null || 
            result.length == 0) {
            console.log("no results found that match");
            // should return an error and redirect but I'll implement that later
            resp.render("accept", {
                printname: "",
                submitted: "",
                material: "",
                cost: "",
                printid: "",
                error: "unable to find print"
            });
        }else{
        console.log("results: " + JSON.stringify(result));
        // quick fix, a join should really be used against the attempts table to select most recent
        // attempts estimate
        let cost = result[0].cost;
        if(cost == null) {
            cost = result[0].estimate;
        }
        
        resp.render("accept", {
            printname: result[0].printname == null ? "unknown" : result[0].printname,
            submitted: result[0].submitted == null ? "unknown" : result[0].submitted,
            material: result[0].material == null ? "unknown" : result[0].material,
            cost: cost == null ? "unknown internal error" : "$" + cost,     
            printid: id == null ? "unkown" : id,
        });
    }
    });
};

router.get("/printable", (req, resp) => {
    client.query("select email, queue.id as id, printname, sliced_file, to_char(submitted, 'mm/dd/yyyy') as submitted, materialname as material, estimate from queue, material_costs where material_costs.id = queue.material and approval = 'accepted'", null, (err, res) => {
        let results = res.rows;
        if(err != null) {
            console.log(err);
        }
        resp.render("prints", {
            electron: false,
            results: results
        });
    });
});

router.get("/uploads/:id", (req, resp) => {
    let id = req.params.id;
    if(id == null) {
        resp.send({
            status: "failure",
            reason: "misisng id"
        });
    }else{
    client.query("select sliced_file,printname from queue where id = $1", [id], (err, res) => {
        if(err != null) {
            console.log(err);
        }
        if(typeof(res) == undefined) {
            res.send({
                status: "failure",
                reason: "query failure likely invalid ID"
            });
        }
        let results = res.rows;
        if(results == null || results.length == 0) {
            resp.send({
                status: "failure",
                reason: "print not found"
            });
        }else{
            resp.header('filename', results[0].printname + ".3mf");
            console.log("sending file: " + results[0].sliced_file);
            resp.sendFile(results[0].sliced_file, null, null);
        }
    });
    }
});

router.get("/approve", (req, resp) => {
    let id = req.query.id;
    if(id == null) {
        resp.send({
            status: "failure",
            reason: "missing id"
        });
    }
    client.query("update queue set approval = 'accepted' where id = $1", [id]);
    resp.render("index", null);
});

router.get("/reject", (req, resp) => {
    let id = req.query.id;
    if(id == null) {
        resp.send({
            status: "failure",
            reason: "missing id"
        });
    }
    client.query("update queue set approval = 'rejected' where id = $1", [id]);
    resp.render("index", null);
});

router.get("/accept", async (req, resp) => {
    let id = req.query.id;
    if(id == null) {
        resp.render("index", null);
    }
    render_accept(id,resp);
});



var server = router.listen(3456, function () {
    var host = server.address().address;
    console.log("address: " + host);
});
