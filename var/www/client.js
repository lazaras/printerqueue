const {app, BrowserWindow} = require('electron');
const ejse = require('ejs-electron');

let mainwindow;

app.on('ready', () => {
    mainwindow = new BrowserWindow();
    mainwindow.loadURL('file://' + __dirname + 'views/prints.ejs');
});