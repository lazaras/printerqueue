create table queue (
id uuid not null,
submitted date,
userid integer,
email varchar(40) not null,
input_file varchar(100) not null,
sliced_file varchar(100),
cost varchar(20),
printable_file varchar(100));
